package com.example.exelartech.restapi;


import android.net.MacAddress;

import com.example.exelartech.retrofit.Loginresponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface WPAPIService {

    @POST("service/register_user.php")
    @FormUrlEncoded
    Call <String> createUser(
            @Field("fname") String firstname ,
            @Field("lname") String lastname ,
            @Field("email_id") String email_address ,
            @Field("password") String password ,
            @Field("mobile") String mobile ,
            @Field("address") String address ,
            @Field("office_no") String office_no ,
            @Field("company") String company ,
            @Field("website") String website,
            @Field ( "madd" ) MacAddress madd,
            @Field ( "madd_from" )String madd_from,
            @Field ( "newsletter" )String newsletter);




    @POST("service/login_user.php")
    @FormUrlEncoded
    Call <String> loginUser(@Field("email_id") String emailid ,
                            @Field("password") String password ,
                            @Field("madd") String madd ,
                            @Field("madd_from") String madd_from);


    @POST("service/get_timezone.php")
    Call <List <String>> getTimeZone();



}


