package com.example.exelartech.restapi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {


    private static final String API_BASE_URL = "https://www.moremovies.com/";

    private static Gson gson = new GsonBuilder ()
            .setLenient ()
            .create ();

    private static OkHttpClient httpClient = new OkHttpClient.Builder ()
            //here we can add Interceptor for dynamical adding headers
            .addNetworkInterceptor ( new Interceptor () {
                @NotNull
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request ().newBuilder ().addHeader ( "test" , "test" ).build ();
                    return chain.proceed ( request );
                }
            } )
            //here we adding Interceptor for full level logging
            .addNetworkInterceptor ( new HttpLoggingInterceptor ().setLevel ( HttpLoggingInterceptor.Level.BODY ) )
            .build ();


    private static Retrofit.Builder builder =
            new Retrofit.Builder ()
                    .baseUrl ( API_BASE_URL )
                    .addConverterFactory ( GsonConverterFactory.create ( gson ) );


    public static <S> S createService(Class <S> serviceClass) {

        //  httpClient.setReadTimeout(60, TimeUnit.SECONDS);
        Retrofit retrofit = builder.client ( httpClient ).build ();




        return retrofit.create ( serviceClass );

    }


}
