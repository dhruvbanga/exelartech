package com.example.exelartech;

import android.net.MacAddress;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.exelartech.restapi.ServiceGenerator;
import com.example.exelartech.restapi.WPAPIService;


import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
   CheckBox newsletter;

    String nameString;
    String lnameString;
    String emailString;
    String passwordString;
    String mobileString;
    String companyString;
    String websiteString;
    String office_noString;
    String madd_fromString;
    String newsletterString;
    MacAddress madd;
    String addressString;

    String selectedTimeZone;

    List <String> timezoneList;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
       setContentView ( R.layout.singup );


//
//        try {
//            WifiManager wifi = (WifiManager) this.getApplicationContext ()
//                    .getSystemService ( WIFI_SERVICE );
//            wifi.setWifiEnabled ( true );
//            WifiInfo info = wifi.getConnectionInfo ();
//
//            String address = info.getMacAddress ();
//
//            if (address == null) {
//                Toast.makeText ( this , "Null" , Toast.LENGTH_LONG ).show ();
//            } else {
//                Toast.makeText ( this , address , Toast.LENGTH_LONG ).show ();
//            }
//        } catch (Exception e) {
//            Toast.makeText ( this , e.toString () , Toast.LENGTH_LONG ).show ();
//        }


        final EditText name = findViewById ( R.id.Firstname );
        final EditText lname = findViewById ( R.id.Lastname );
        final EditText email = findViewById ( R.id.email );
        final EditText password = findViewById ( R.id.password );
        final EditText mobile = findViewById ( R.id.mobile );
        final EditText officeno = findViewById ( R.id.officeno );
        final EditText website = findViewById ( R.id.website );
        final EditText address = findViewById ( R.id.address );
        final EditText company = findViewById ( R.id.company );

        Button singupButton = findViewById ( R.id.singup );

        final Spinner timeZoneSpinner = findViewById ( R.id.timeZone );

        timeZoneSpinner.setOnItemSelectedListener ( new AdapterView.OnItemSelectedListener () {
            @Override
            public void onItemSelected(AdapterView <?> parent , View view , int position , long id) {
                Toast.makeText ( getApplicationContext () , timezoneList.get ( position ) , Toast.LENGTH_LONG ).show ();

                selectedTimeZone = timezoneList.get ( position );

            }

            @Override
            public void onNothingSelected(AdapterView <?> parent) {

            }
        } );


        ServiceGenerator.createService ( WPAPIService.class ).getTimeZone ().enqueue ( new Callback <List <String>> () {
            @Override
            public void onResponse(Call <List <String>> call , @NotNull Response <List <String>> response) {

                timezoneList = response.body ();
                ArrayAdapter <String> aa = new ArrayAdapter <> ( MainActivity.this , android.R.layout.simple_spinner_item , timezoneList );
                aa.setDropDownViewResource ( android.R.layout.simple_spinner_dropdown_item );
                //Setting the ArrayAdapter data on the Spinner
                timeZoneSpinner.setAdapter ( aa );


            }

            @Override
            public void onFailure(Call <List <String>> call , Throwable t) {

            }
        } );


        singupButton.setOnClickListener ( new OnClickListener () {
            @Override
            public void onClick(View v) {

                nameString = name.getText ().toString ();
                lnameString = lname.getText ().toString ();
                emailString = email.getText ().toString ();
                passwordString = password.getText ().toString ();
                mobileString = mobile.getText ().toString ();
                addressString = address.getText ().toString ();
                office_noString = officeno.getText ().toString ();
                websiteString = website.getText ().toString ();
                companyString = company.getText ().toString ();
                madd.getAddressType ();


                ApiCall ();
            }
        } );


    }


    void ApiCall() {


        ServiceGenerator.createService ( WPAPIService.class )
                .createUser ( nameString , lnameString , emailString , passwordString , mobileString , addressString , office_noString , companyString , websiteString , madd , madd_fromString , newsletterString )
                .enqueue ( new Callback <String> () {
                    @Override
                    public void onResponse(@NotNull Call <String> call , @NotNull Response <String> response) {
                        Toast.makeText ( MainActivity.this , response.body () , Toast.LENGTH_LONG ).show ();

                    }

                    @Override
                    public void onFailure(@NotNull Call <String> call , Throwable t) {
                        Toast.makeText ( MainActivity.this , t.getMessage () , Toast.LENGTH_LONG ).show ();

                    }
                } );

    }

    }

