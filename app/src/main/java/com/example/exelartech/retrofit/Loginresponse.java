
package com.example.exelartech.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Loginresponse {



    @SerializedName("0")
    @Expose
    private String _0;
    @SerializedName("Sno")
    @Expose
    private String sno;
    @SerializedName("1")
    @Expose
    private String _1;
    @SerializedName("subscribed")
    @Expose
    private String subscribed;
    @SerializedName("2")
    @Expose
    private String _2;
    @SerializedName("status")
    @Expose
    private String status;

    public String get0() {
        return _0;
    }

    public void set0(String _0) {
        this._0 = _0;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String get1() {
        return _1;
    }

    public void set1(String _1) {
        this._1 = _1;
    }

    public String getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(String subscribed) {
        this.subscribed = subscribed;
    }

    public String get2() {
        return _2;
    }

    public void set2(String _2) {
        this._2 = _2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
