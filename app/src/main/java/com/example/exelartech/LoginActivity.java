package com.example.exelartech;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.exelartech.restapi.ServiceGenerator;
import com.example.exelartech.restapi.WPAPIService;
import com.example.exelartech.retrofit.Loginresponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {


    String emailString;
    String PasswordString;
    String macAddressString;
    String macAddressFromString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.login );


        final EditText emailEditText = findViewById ( R.id.email );
        final EditText passwordEditText = findViewById ( R.id.password );
        final EditText MacAddressEditText = findViewById ( R.id.MacAddress );
        final EditText MACAddressfromEditText = findViewById ( R.id.MACAddressfrom );


        Button singupButton = findViewById ( R.id.singup );

        Button logginButton = findViewById ( R.id.loggin );

        logginButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {


                emailString = emailEditText.getText ().toString ();
                PasswordString = passwordEditText.getText ().toString ();
                macAddressString = MacAddressEditText.getText ().toString ();
                macAddressFromString = MACAddressfromEditText.getText ().toString ();

                ApiCallLogin ();
            }
        } );

        singupButton.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {


                startActivity ( new Intent ( LoginActivity.this , MainActivity.class ) );
            }
        } );

    }


    void ApiCallLogin() {
        ServiceGenerator.createService ( WPAPIService.class )
                .loginUser ( emailString,PasswordString,macAddressString,macAddressFromString).enqueue ( new Callback <String> () {
            @Override
            public void onResponse(Call <String> call , Response <String> response) {
                Toast.makeText ( LoginActivity.this , response.body () , Toast.LENGTH_LONG ).show ();

            }

            @Override
            public void onFailure(Call <String> call , Throwable t) {
                Toast.makeText ( LoginActivity.this , t.getMessage () , Toast.LENGTH_LONG ).show ();

            }
        } );


    }


//
//    List<Loginresponse> loginresponse=response.body ();
//
//                if(loginresponse!=null && loginresponse.size ()>0)
//    {
//
//        Toast.makeText ( LoginActivity.this,"Login has been done. Go to home page",Toast.LENGTH_LONG ).show ();
//
//    }else
//    {
//
//        Toast.makeText ( LoginActivity.this,"Please check email id and password",Toast.LENGTH_LONG ).show ();
//
//
//    }


}



